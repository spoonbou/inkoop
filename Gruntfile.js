
module.exports = function(grunt) {

    grunt.initConfig({

        wiredep: {
            target: {
                src: 'webapp/index.html'
            }
        },

        nodemon: {
            script: 'index.js'
        }


    });


    grunt.loadNpmTasks('grunt-wiredep');
    grunt.loadNpmTasks('grunt-nodemon');

    // grunt.registerTask('wiredep', [ 'wiredep' ]);
    grunt.registerTask('default', [ 'wiredep', 'nodemon' ]);

};