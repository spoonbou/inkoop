

var express         = require('express'),
    app             = express(),
    path            = require('path');


app.listen(8000, '0.0.0.0');
console.log('');
console.log(' ... server online on 8000 ... ');


app.use(express.static(path.join(__dirname) + '/webapp'));


app.get('/', function(req, res) {
    res.render('index.html');
});
