
(function() {

    'use strict';

    angular.module('inkoop').controller("categoryController", categoryController);

    categoryController.$inject = ['$scope', '$mdDialog', 'locals'];

    function categoryController($scope, $mdDialog, locals) {

        $scope.addBool = false;

        $scope.buttonTitle = locals.buttonTitle;
        $scope.categoryTitle = 'Add Category';
        $scope.types = ['type 1', 'type 2', 'type 3', 'type 4'];
        $scope.category = {
            icon    : '',
            title   : '',
            type    : '',
            description: ''
        };

        init();
        function init() {

            // View
            if (locals.category && locals.view) {
                $scope.categoryTitle = 'View Category';
                $scope.category = locals.category;
            } else {

                // Edit
                if (locals.category) {
                    $scope.categoryTitle = 'Update Category';
                    $scope.category = angular.copy(locals.category);
                    console.log(locals.category);
                } else {
                    // Create

                }


            }
        }

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.categoryAction = function(category) {

            switch ($scope.buttonTitle) {
                case 'view':
                    $mdDialog.hide();
                    break;
                case 'create':
                    $mdDialog.hide(category);
                    break;
                case 'update':
                    $mdDialog.hide(category);
                    break;
            }

        };

    }

})();