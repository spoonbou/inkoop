
(function() {

    'use strict';

    angular.module('inkoop').controller("categoriesController", categoriesController);

    categoriesController.$inject = ['$scope', '$log', '$mdDialog', 'promiseObj', '$http'];

    function categoriesController($scope, $log, $mdDialog, promiseObj, $http) {

        //$scope.type1 = false;
        //$scope.type2 = false;
        //$scope.type3 = false;

        $scope.hoverIn = function(){
            this.hoverEdit = true;
        };

        $scope.hoverOut = function(){
            this.hoverEdit = false;
        };

        $scope.searchParam = '';

        $scope.categories = promiseObj.data.categories;

        //$scope.getCategories = function () {
        //    $scope.categoriesPromise = $http({method: 'GET', url: '/data/categories.json'});
        //};


        $scope.query = {
            order: 'title',
            limit: 5,
            page: 1
        };

        $scope.viewCategory = function(category) {
            $mdDialog.show({
                controller: 'categoryController',
                templateUrl: 'components/categories/category/categoryView.html',
                locals: {
                    category: category,
                    view    : true,
                    buttonTitle : 'close'
                },
                clickOutsideToClose:false
            })
                .then(function() {
                    $log.info('Viewed');
                }, function(error) {
                    $log.error(error);
                });
        };

        $scope.updateCategory = function(category, idx) {
            $mdDialog.show({
                controller: 'categoryController',
                templateUrl: 'components/categories/category/categoryView.html',
                locals: {
                    category    : category,
                    view        : false,
                    buttonTitle : 'update'
                },
                clickOutsideToClose:false
            })
                .then(function(categoryUpdate) {
                    categoryUpdate.datem = new Date().toLocaleString().replace(',', '');
                    $scope.categories[idx] = categoryUpdate;
                }, function(error) {
                    $log.error(error);
                });
        };

        $scope.createCategory = function() {
            $mdDialog.show({
                controller: 'categoryController',
                templateUrl: 'components/categories/category/categoryView.html',
                locals: {
                    buttonTitle : 'create'
                },
                clickOutsideToClose:false
            })
                .then(function(category) {
                    category.datec = new Date().toLocaleString().replace(',','');
                    category.datem = new Date().toLocaleString().replace(',','');
                    $scope.categories.push(category);
                }, function(error) {
                    $log.error(error);
                });
        };

        $scope.confirmDelete = function(category, e) {
            e.stopPropagation();
            $mdDialog.show({
                controller: 'confirmDialogController',
                templateUrl: 'components/categories/confirm/confirmDialogView.html',
                locals: {
                    title: category.title
                },
                clickOutsideToClose:false
            })
                .then(function() {
                    $scope.categories.splice( $scope.categories.indexOf(category), 1 );
                }, function() {
                    $log.info('You cancelled the dialog.');
                });
        };


    }

})();
