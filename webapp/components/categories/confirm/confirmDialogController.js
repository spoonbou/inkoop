
(function() {

    'use strict';

    angular.module('inkoop').controller("confirmDialogController", confirmDialogController);

    confirmDialogController.$inject = ['$scope', '$mdDialog', 'locals'];

    function confirmDialogController($scope, $mdDialog, locals) {

        $scope.title = locals.title;

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.remove = function() {
            //$scope.post.createdOn = myDate;
            $mdDialog.hide();
        };

    }

})();