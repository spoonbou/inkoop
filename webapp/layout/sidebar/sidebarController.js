
(function() {

    'use strict';

    angular.module('inkoop').controller("sidebarController", sidebarController);

    sidebarController.$inject = ['$scope', '$location', '$mdSidenav'];

    function sidebarController($scope, $location, $mdSidenav) {

        $scope.closeNav = function(navID) {
            if (!$mdSidenav(navID).isLockedOpen()) {
                $mdSidenav(navID)
                    .toggle();
            }
        };

        $scope.isCurrentPath = function (path) {
            return $location.path() == path;
        };

    }

})();
