
(function() {

    'use strict';

    angular.module('inkoop').controller("headerController", headerController);

    headerController.$inject = ['$scope', '$mdSidenav'];

    function headerController($scope, $mdSidenav) {

        $scope.openMenu = function(navID) {
            $mdSidenav(navID)
                .toggle();
        }

    }

})();