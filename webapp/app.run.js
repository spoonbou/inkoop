/**
 * Created on 11/18/2015.
 * @author m.kerkez
 */
angular
    .module("sprinkle")
    .run(["$rootScope",
        function ($rootScope) {

            $rootScope.$on('$stateChangeStart', function(event, toState) {
                if (toState.data.title) {
                    $rootScope.title = toState.data.title;
                }
            });

        }]);