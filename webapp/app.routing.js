
(function() {

    'use strict';

    angular.module("inkoop")
        .config(["$stateProvider", "$urlRouterProvider",
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/');

                $stateProvider

                    .state('home', {
                        url:'/',
                        views:{
                            'header': {
                                templateUrl:'layout/header/headerView.html',
                                controller: 'headerController'
                            },
                            'sidebar': {
                                templateUrl:'layout/sidebar/sidebarView.html',
                                controller: 'sidebarController'
                            },
                            'content': {
                                templateUrl:'components/dashboard/dashboardView.html',
                                controller: 'dashboardController'
                            }
                        }
                    })

                    .state('home.categories', {
                        url:'categories',
                        views:{
                            'content@': {
                                templateUrl: 'components/categories/categoriesView.html',
                                controller: 'categoriesController'
                            }
                        },
                        resolve: {
                            promiseObj:  function($http){
                                return $http({method: 'GET', url: '/data/categories.json'});
                            }
                        }
                    })

                    .state('home.users', {
                        url:'users',
                        views:{
                            'content@': {
                                templateUrl: 'components/users/usersView.html'
                            }
                        }
                    });

            }
        ]);
}());