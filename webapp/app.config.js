
(function () {

    'use strict';

    angular.module("inkoop").config(config);

    config.$inject = ['$mdThemingProvider'];

    function config($mdThemingProvider){

        $mdThemingProvider.theme('docs-dark', 'default')
            .primaryPalette('blue')
            .accentPalette('deep-orange')
            .dark();

    }



}());