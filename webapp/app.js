
(function(){

    'use strict';

    angular.module('inkoop', [

        'ngAnimate',
        'ngMaterial',
        'md.data.table',
        'ui.router'

        //'ngResource',
        //'pascalprecht.translate',
        // 'ngLodash',
        // 'angular.filter',
        // 'ui.select',
        // 'ngCountTo',
        // 'ngFileUpload'

    ]);

}());
